import Navbar from './nav'
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPaige';
import { BrowserRouter, Routes, Route  } from 'react-router-dom'
import { useState, useEffect } from 'react';


function App(props) {

  const [attendees, setAttendees] = useState([]);
  const [locations, setLocations] = useState([]);
  const [conferences, setConferences] = useState([]);


  async function getAttendees() {
    const response = await fetch('http://localhost:8001/api/attendees/');
    
    if (response.ok) {
      const {attendees} = await response.json();
      setAttendees(attendees);
    } else {
      console.error('An error occurred while fetching the data')
    }
  }

  const getConferences = async () => {
    const response = await fetch('http://localhost:8000/api/conferences/');
    if (response.ok) {
      const {conferences} = await response.json();
      setConferences(conferences);
    }else{
      console.error('An error occurred while fetching the data')
    }
  }

  useEffect(() => {
    getAttendees();
    getConferences();
  }, [])

  if (attendees === undefined) {
    return null;
  }

  return (
    <BrowserRouter>
        <Navbar />
        <div className="container">
          <Routes>
            <Route index element={<MainPage conferences={conferences} />} />
            <Route path="locations">
              <Route path="new" element={<LocationForm />} />
            </Route>
            <Route path="attendees">
              <Route index element={<AttendeesList attendees={attendees} />} />
              <Route path="new" element={<AttendConferenceForm conferences={conferences} getAttendees={getAttendees} />} />
            </Route>
            <Route path='conferences'>
              <Route path="new" element={<ConferenceForm getConferences={getConferences} />} />
            </Route>
            <Route path='presentations'>
              <Route path="new" element={<PresentationForm conferences={conferences} />} />
            </Route>
          </Routes>
        </div>
    </BrowserRouter>
  );
}

export default App;