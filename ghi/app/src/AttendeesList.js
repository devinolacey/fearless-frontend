import React, { Fragment } from "react";

function AttendeesList ({attendees}) {

    console.log(attendees)
    return (
        <Fragment>
            <table className="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Conference</th>
                </tr>
                </thead>
                <tbody>
                {attendees.map(attendee => {
                    return (
                    <tr key={attendee.href}>
                        <td>{attendee.name}</td>
                        <td>{attendee.conference}</td>
                    </tr>
                    );
                })}
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </Fragment>
    )
}
export default AttendeesList