import React, { useEffect, useState } from "react";

function ConferenceForm ({getConferences}) {
    const [locations, setLocations] = useState([]);
    
    const [name, setName] = useState('');
    const handleNameChange =(e) => {
        setName(e.target.value);
    }
    
    const [starts, setStarts] = useState('');
    const handleStartsChange =(e) => {
        setStarts(e.target.value);
    }

    const [ends, setEnds] = useState('');
    const handleEndsChange =(e) => {
        setEnds(e.target.value);
    }

    const [description, setDescription] = useState('');
    const handleDescriptionChange =(e) => {
        setDescription(e.target.value);
    }

    const [maxPresentations, setMaxPresentations] = useState('');
    const handleMaxPresentationsChange =(e) => {
        setMaxPresentations(e.target.value);
    }

    const [maxAttendees, setMaxAttendees] = useState('');
    const handleMaxAttendeesChange =(e) => {
        setMaxAttendees(e.target.value);
    }

    const [location, setLocation] = useState('');
    const handleLocationChange =(e) => {
        setLocation(e.target.value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data ={};

        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;

        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/'
                const fetchConfig = {
                    method: 'post',
                    body: JSON.stringify(data),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(conferenceUrl, fetchConfig);
                if (response.ok) {
                    const newConference = await response.json();
                    setName('');
                    setStarts('');
                    setEnds('');
                    setDescription('');
                    setMaxPresentations('');
                    setMaxAttendees('');
                    setLocation('');
                    getConferences();
                }
    }

    const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
    }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartsChange} value={starts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"/>
                <label htmlFor="starts">Start Date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndsChange} value={ends} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control"/>
                <label htmlFor="ends">End Date</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description" className="form-label">Description</label>
                <textarea onChange={handleDescriptionChange} value={description} placeholder="Description" required name="description" id="description" className="form-control"></textarea>
            </div>
            
              <div className="form-floating mb-3">
                <input onChange={handleMaxPresentationsChange} value={maxPresentations} placeholder="Max presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                <label htmlFor="max_presentations">Max presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxAttendeesChange} value={maxAttendees} placeholder="Max attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                <label htmlFor="max_attendees">Max attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} value={location} required id="location" name= "location" className="form-select">
                    <option value="">Choose a location</option>
                    {locations.map(location => {
                        return (
                            <option key={location.id} value={location.id}>
                                {location.name}
                            </option>
                        );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    )
}
export default ConferenceForm